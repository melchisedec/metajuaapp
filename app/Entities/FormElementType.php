<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

class FormElementType {

    public $id;
    public $name;
    public $taxonomy_id;

    public $blueprint;

    public function __construct( $identifier ) {
        $term = new Term( 'Form element type', $identifier );

        if ( $term->id != null ) :

            $this->id = $term->id;
            $this->name = $term->name;
            
            $this->taxonomy_id = $term->taxonomy_id;

            $meta = new TermMeta( $this->id, 'blueprint' );
            $this->blueprint = $meta->meta_value;

        endif;
    }

    public static function add( $name = '', $blueprint = '' ) {
        if ( empty( $name ) )
            return null;

        $term_id = Term::add( 'Form element type', $name );

        TermMeta::add( $term_id, 'blueprint', $blueprint );

        return $term_id;
    }

    public static function get_form_element_types( $per_page = 0, $page = 0 ) {

        $taxonomy = new Taxonomy( 'Form element type' );

        if ( $taxonomy->id != null ) :

            if ( $per_page == 0 && $page == 0 ) :

                $records = DB::select( "SELECT id FROM terms WHERE taxonomy_id = :taxonomy_id ORDER BY name ASC", [
                    'taxonomy_id' => $taxonomy->id
                ] );

            else :

                $page = $per_page > 0 && $page == 0 ? 1 : $page;
                $per_page = $page > 0 && $per_page == 0 ? 15 : $per_page;

                $records = DB::select(
                    "SELECT id FROM terms WHERE taxonomy_id = :taxonomy_id ORDER BY name ASC LIMIT :lower_limit, :per_page", [
                        'taxonomy_id' => $taxonomy->id,
                        'lower_limit' => ( $page - 1 ) * $per_page,
                        'per_page' => $per_page
                    ]
                );

            endif;

            foreach( $records as $record )
                $form_element_types[] = new FormElementType( $record->id );

        endif;

        return isset( $form_element_types ) ? $form_element_types : [];
    }

}