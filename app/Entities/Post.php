<?php

namespace App\Entities;

use App\Models\Posts;

class Post {

	public $id;
	public $post_type_id;
	public $user_id;
	public $name;
	public $parent_id;
	public $meta = [];

	public function __construct( $post_type, $identifier, $user_id = null ) {
		$post_type = new PostType( $post_type );

		if ( $post_type->id != null ) :

			$query = Posts::where( 'post_type_id', '=', $post_type->id );

			is_numeric( $identifier ) ?
				$query->where( 'id', '=', $identifier ) :
				$query->where( 'name', 'LIKE', strtolower( $identifier ) );

			$user_id != null ? $query->where( 'user_id', '=', $user_id ) : null;

			$record = $query->first();

			if ( !empty( $record ) ) :

				$this->id = $record->id;
				$this->post_type_id = $record->post_type_id;
				$this->user_id = $record->user_id;
				$this->name = $record->name;
				$this->parent_id = $record->parent_id;

			endif;

		endif;
	}

	public static function add( $post_type, $name, $parent_id = null, $user_id = null ) {
		if ( empty( $name ) )
			return null;

		$post_type = new PostType( $post_type );

		if ( $post_type->id == null )
			return null;

		$post = new Post( $post_type->id, $name, $user_id );

		if ( $post->id != null && $post->parent_id == $parent_id )
			return $post->id;

		$post = new Posts();
		$post->post_type_id = $post_type->id;
        $parent_id != null ? $post->parent_id = $parent_id : null;
		$user_id != null ? $post->user_id = $user_id : null;
		$post->name = $name;
		$post->save();

		return $post->id;
	}

}