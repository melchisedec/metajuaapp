<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

class FormElementResponse {

	public $id;
	public $name;
	public $submission;
	public $form_element;
	public $response;

	public function __construct( $identifier ) {
		$post = new Post( 'Form element response', $identifier );

		if ( $post->id != null ) :

			$this->id = $post->id;
		    $this->name = $post->name;
            $this->submission = $post->parent_id;

            $meta = new PostMeta( $this->id, 'element' );
            $this->form_element = $meta->meta_value;

            $meta = new PostMeta( $this->id, 'response' );
            $this->response = $meta->meta_value;

		endif;
	}

	public static function add( $submission_id = null, $name = null, $form_element_id = null, $response = null, $user_id = null ) {
		if (  $submission_id == null || $name == null || $form_element_id == null || $user_id == null )
			return null;

        $post_id = Post::add( 'Form element response', $name, $submission_id, $user_id );

        PostMeta::add( $post_id, 'element', $form_element_id );
        PostMeta::add( $post_id, 'response', $response );

        return $post_id;
	}

    public static function get_form_element_responses( $submissions = [], $per_page = 0, $page = 0 ) {
	    $post_type = new PostType( 'Form element response' );

	    if ( $post_type->id != null ) :

            if ( empty( $submissions ) && $per_page == 0 && $page == 0 ) :

                $records = DB::select( "SELECT id FROM posts WHERE post_type_id = :post_type_id ORDER BY name ASC", [
                    'post_type_id' => $post_type->id
                ] );

            else :

                $page = $per_page > 0 && $page == 0 ? 1 : $page;
                $per_page = $page > 0 && $per_page == 0 ? 15 : $per_page;

                if ( !empty( $submissions ) && $per_page == 0 && $page == 0 ) :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND posts.parent_id IN ( " . implode( ', ', $submissions ) . " ) AND post_meta.meta_key = :response", [
                            'post_type_id' => $post_type->id,
                            'response' => 'response'
                        ]
                    );

                elseif ( empty( $submissions ) && $per_page > 0 ) :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND post_meta.meta_key = :response LIMIT :lower_limit, :per_page", [
                            'post_type_id' => $post_type->id,
                            'response' => 'response',
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                else :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND posts.parent_id IN ( " . implode( ', ', $submissions ) . " ) AND post_meta.meta_key = :response LIMIT :lower_limit, :per_page", [
                            'post_type_id' => $post_type->id,
                            'response' => 'response',
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                endif;

            endif;

            foreach( $records as $record )
                $form_element_responses[] = new FormElementResponse( $record->id );

        endif;

        return isset( $form_element_responses ) ? $form_element_responses : [];
    }

}