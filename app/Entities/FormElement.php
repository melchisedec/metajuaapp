<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

class FormElement {

	public $id;
	public $name;
	public $type;
	public $form_section;
	public $meta_data;

	public function __construct( $identifier ) {
		$post = new Post( 'Form element', $identifier );

		if ( $post->id != null ) :

			$this->id = $post->id;
			$this->name = $post->name;

            $meta = new PostMeta( $this->id, 'type' );
            $this->type = $meta->meta_value;

            $this->form_section = $post->parent_id;

            $this->meta_data = PostMeta::get_post_meta( $this->id );

		endif;
	}

	public static function add( $form_section_id = null, $name = '', $type = '', $meta_data = [], $user_id = null ) {
		if ( empty( $name ) || $form_section_id == null || empty( $type ) )
			return null;

        $type = new FormElementType( $type );

        if ( $type->id == null )
            return null;

        $blueprint = "App\Blueprints\\" . $type->blueprint;

        $defaults = call_user_func( [ $blueprint, 'get_defaults' ] );

        foreach( $defaults as $key => $value )

            if ( !isset( $meta_data[ $key ] ) )
                $meta_data[ $key ] = $value;

        if ( !call_user_func( [ $blueprint, 'validate' ], $meta_data ) )
            return null;

        $post_id = Post::add( 'Form element', $name, $form_section_id, $user_id );

        PostMeta::add( $post_id, 'type', $type->id );

        foreach( $meta_data as $meta_key => $meta_value )
            PostMeta::add( $post_id, $meta_key, $meta_value );

        return $post_id;
	}

    public static function get_form_elements( $form_sections = [], $per_page = 0, $page = 0 ) {
	    $post_type = new PostType( 'Form element' );

	    if ( $post_type->id != null ) :

            if ( empty( $form_sections ) && $per_page == 0 && $page == 0 ) :

                $records = DB::select( "SELECT id FROM posts WHERE post_type_id = :post_type_id ORDER BY name ASC", [
                    'post_type_id' => $post_type->id
                ] );

            else :

                $page = $per_page > 0 && $page == 0 ? 1 : $page;
                $per_page = $page > 0 && $per_page == 0 ? 15 : $per_page;

                if ( !empty( $form_sections ) && $per_page == 0 && $page == 0 ) :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND posts.parent_id IN ( " . implode( ', ', $form_sections ) . " ) AND post_meta.meta_key = :order ORDER BY post_meta.meta_value ASC", [
                            'post_type_id' => $post_type->id,
                            'order' => 'order'
                        ]
                    );

                elseif ( empty( $form_sections ) && $per_page > 0 ) :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND post_meta.meta_key = :order ORDER BY post_meta.meta_value ASC LIMIT :lower_limit, :per_page", [
                            'post_type_id' => $post_type->id,
                            'order' => 'order',
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                else :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND posts.parent_id IN ( " . implode( ', ', $form_sections ) . " ) AND post_meta.meta_key = :order ORDER BY post_meta.meta_value ASC LIMIT :lower_limit, :per_page", [
                            'post_type_id' => $post_type->id,
                            'order' => 'order',
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                endif;

            endif;

            foreach( $records as $record )
                $form_elements[] = new FormElement( $record->id );

        endif;

        return isset( $form_elements ) ? $form_elements : [];
    }

}