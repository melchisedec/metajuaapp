<?php

namespace App\Entities;

use App\Models\Terms;

class TermMeta {

	public $id;
	public $term_id;
	public $meta_key;
	public $meta_value;

	public function __construct( $term_id, $identifier ) {
		$query = \App\Models\TermMeta::where( 'term_id', '=', $term_id );

		is_numeric( $identifier ) ?
			$query->where( 'id', '=', $identifier ) :
			$query->where( 'meta_key', 'LIKE', strtolower( $identifier ) );

		$record = $query->first();

		if ( !empty( $record ) ) :

			$this->id = $record->id;
			$this->term_id = $record->term_id;
			$this->meta_key = $record->meta_key;

			$first_char = substr( $record->meta_value, 0, 1 );
			$last_char = substr( $record->meta_value, ( strlen( $record->meta_value ) - 1 ) );

            $this->meta_value = in_array( $first_char, [ '{', '[' ] ) && in_array( $last_char, [ '}', ']' ] ) ? json_decode( $record->meta_value ) : $record->meta_value;

		endif;
	}

	public static function add( $term_id, $key, $value = '' ) {
		if ( empty( $key ) )
			return null;

		$term = Terms::find( $term_id );

		if ( empty( $term ) )
			return null;

		$meta = new TermMeta( $term->id, $key );

		if ( $meta->term_id != null )
			return TermMeta::update( $term->id, $key, $value );

		$meta = new \App\Models\TermMeta();
		$meta->term_id = $term->id;
		$meta->meta_key = $key;
        $meta->meta_value = is_array( $value ) ? json_encode( $value ) : $value;
        $meta->save();

        $meta->meta_value = json_decode( $meta->meta_value );

		return $meta->id;
	}

	public static function update( $term_id, $key, $value = '' ) {
		if ( empty( $key ) )
			return null;

		$term = Terms::find( $term_id );

		if ( empty( $term ) )
			return null;

		$meta = new TermMeta( $term->id, $key );

		if ( $meta->term_id == null )
			return TermMeta::add( $term->id, $key, $value );

		$record = \App\Models\TermMeta::where( 'term_id', '=', $term->id )
			->where( 'meta_key', '=', $key )
			->first();

		$record->meta_value = is_array( $value ) ? json_encode( $value ) : $value;
		$record->save();

		$record->meta_value = json_decode( $record->meta_value );

		return $record->id;
	}

}