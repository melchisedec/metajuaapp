<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\Models\PostTypes;

class PostType {

	public $id;
	public $name = [];

	public function __construct( $identifier ) {
		$record = PostTypes::where(
			( is_numeric( $identifier ) ? 'id' : 'singular' ), '=', $identifier
		)->first();

		if ( !empty( $record ) ) :

			$this->id = $record->id;
			$this->name = [
				'singular' => $record->singular,
				'plural' => $record->plural
			];

		endif;
	}

	public static function add( $singular = '', $plural = '' ) {
		if ( empty( $singular ) || empty( $plural ) )
			return null;

		$post_type = new PostType( $singular );

		if ( $post_type->id != null )
			return $post_type->id;

		$post_type = new PostTypes();
        $post_type->singular = $singular;
        $post_type->plural = $plural;
        $post_type->save();

		return $post_type->id;
	}

    public static function get_post_types( $identifier ) {
        $records = DB::select( "SELECT id, singular, plural FROM post_types WHERE singular = :value ORDER BY id ASC LIMIT 1", [
            'value' => $identifier
        ] );

        return !empty( $records ) ? $records : [];
    }


}