<?php

namespace App\Entities;

use App\Models\Posts;
use Illuminate\Support\Facades\DB;

class PostMeta {

	public $id;
	public $post_id;
	public $meta_key;
	public $meta_value;

	public function __construct( $post_id, $identifier ) {
		$query = \App\Models\PostMeta::where( 'post_id', '=', $post_id );

		is_numeric( $identifier ) ?
			$query->where( 'id', '=', $identifier ) :
			$query->where( 'meta_key', 'LIKE', strtolower( $identifier ) );

		$record = $query->first();

		if ( !empty( $record ) ) :

			$this->id = $record->id;
			$this->post_id = $record->post_id;
			$this->meta_key = $record->meta_key;

            $this->meta_value = json_decode( $record->meta_value ) != null ? json_decode( $record->meta_value ) : $record->meta_value;

		endif;
	}

	public static function add( $post_id, $key, $value = '' ) {
		if ( empty( $key ) )
			return null;

		$post = Posts::find( $post_id );

		if ( empty( $post ) )
			return null;

		$meta = new PostMeta( $post->id, $key );

		if ( $meta->post_id != null )
			return PostMeta::update( $post->id, $key, $value );

		$meta = new \App\Models\PostMeta();
		$meta->post_id = $post->id;
		$meta->meta_key = $key;
        $meta->meta_value = is_array( $value ) ? json_encode( $value ) : $value;
        $meta->save();

        $meta->meta_value = json_decode( $meta->meta_value );

		return $meta->id;
	}

	public static function update( $post_id, $key, $value = '' ) {
		if ( empty( $key ) )
			return null;

		$post = Posts::find( $post_id );

		if ( empty( $post ) )
			return null;

		$meta = new PostMeta( $post->id, $key );

		if ( $meta->post_id == null )
			return PostMeta::add( $post->id, $key, $value );

		$record = \App\Models\PostMeta::where( 'post_id', '=', $post->id )
			->where( 'meta_key', '=', $key )
			->first();

        $record->meta_value = is_array( $value ) ? json_encode( $value ) : $value;
        $record->save();

        $record->meta_value = json_decode( $record->meta_value );

		return $record->id;
	}

	public static function get_post_meta( $post_id = null ) {

	    if ( $post_id == null )
	        return [];

       $records = \App\Models\PostMeta::where( 'post_id', '=', $post_id )->get();

        if ( !empty( $records ) )

            foreach( $records as $record )
                $meta_data[ $record->meta_key ] = $record->meta_value;

        return isset( $meta_data ) ? $meta_data : [];
    }

}