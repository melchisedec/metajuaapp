<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

use App\Models\Users;
use App\Models\UserMeta;

class User {

	public $id;
	public $username;
	public $name = [];
	public $meta = [];

	public function __construct( $identifier ) {
		$record =  Users::where(
			( is_numeric( $identifier ) ? 'id' : 'username' ), '=', $identifier
		)->first();

		if ( !empty( $record ) ) :

			$this->id = $record->id;
			$this->username = $record->username;

			$records = UserMeta::where( 'user_id', '=', $this->id )->get();

			foreach( $records as $meta ) :

				if ( in_array( $meta->meta_key, [ 'first_name', 'last_name' ] ) )
					$this->name[ str_replace( '_name', '', $meta->meta_key ) ] = $meta->meta_value;

				else
					$this->meta[ $meta->meta_key ] = $meta->meta_value;

			endforeach;

		endif;
	}

	public static function add( $params = [] ) {
		$required_fields = [ 'username', 'password' ];

		foreach( $required_fields as $field )

			if ( !isset( $params[ $field ] ) )
				return false;

		$params = ( object ) $params;

		$user = new Users();
		$user->username = $params->username;
		$user->password = $params->password;
		$user->save();

		foreach( ( isset( $params->meta_data ) ? $params->meta_data : [] ) as $key => $value ) :

			$meta = new UserMeta();
			$meta->user_id = $user->id;
			$meta->meta_key = $key;
			$meta->meta_value = $value;
			$meta->save();

		endforeach;

		return $user->id;
	}

	public static function get_users( $accounts = [], $per_page = 0, $page = 0 ) {
        if ( empty( $accounts ) && $per_page == 0 && $page == 0 ) :

            $records = DB::select( "SELECT id FROM users ORDER BY id ASC", [] );

		else :

            $page = $per_page > 0 && $page == 0 ? 1 : $page;
            $per_page = $page > 0 && $per_page == 0 ? 15 : $per_page;

            if ( !empty( $accounts ) && $per_page == 0 && $page == 0 ) :

                $records = DB::select(
                    "SELECT users.id AS id FROM users LEFT JOIN user_meta ON users.id = user_meta.user_id WHERE user_meta.meta_key = :account AND user_meta.meta_value IN ( " . implode( ', ', $accounts ) . " ) ORDER BY users.id ASC", [
                        'account' => 'account'
                    ]
                );

            elseif ( empty( $accounts ) && $per_page > 0 ) :

                $records = DB::select(
                    "SELECT id FROM users ORDER BY id ASC LIMIT :lower_limit, :per_page", [
                        'lower_limit' => ( $page - 1 ) * $per_page,
                        'per_page' => $per_page
                    ]
                );

            else :

                $records = DB::select(
                    "SELECT users.id AS id FROM users LEFT JOIN user_meta ON users.id = user_meta.user_id WHERE user_meta.meta_key = :account AND user_meta.meta_value IN ( " . implode( ', ', $accounts ) . " ) ORDER BY users.id ASC LIMIT :lower_limit, :per_page", [
                        'account' => 'account',
                        'lower_limit' => ( $page - 1 ) * $per_page,
                        'per_page' => $per_page
                    ]
                );

            endif;

        endif;

		foreach( $records as $record )
			$users[] = new User( $record->id );

		return isset( $users ) ? $users : [];
	}

}