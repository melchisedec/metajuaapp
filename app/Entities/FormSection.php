<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

class FormSection {

	public $id;
	public $name;
	public $form;
	public $order;
	public $hidden;

	public function __construct( $identifier ) {
		$post = new Post( 'Form section', $identifier );

		if ( $post->id != null ) :

			$this->id = $post->id;
			$this->name = $post->name;

            $this->form = $post->parent_id;

            $meta = new PostMeta( $this->id, 'order' );
            $this->order = $meta->meta_value;

            $meta = new PostMeta( $this->id, 'hidden' );
            $this->hidden = $meta->meta_value;

		endif;
	}

	public static function add( $form_id = null, $name = '', $order = 1, $hidden = false, $user_id = null ) {
		$post_id = Post::add( 'Form section', $name, $form_id, $user_id );

        PostMeta::add( $post_id, 'order', $order );
        PostMeta::add( $post_id, 'hidden', $hidden );

        return $post_id;
	}

    public static function get_form_sections( $forms = [], $per_page = 0, $page = 0 ) {
	    $post_type = new PostType( 'Form section' );

	    if ( $post_type->id != null ) :

            if ( empty( $forms ) && $per_page == 0 && $page == 0 ) :

                $records = DB::select( "SELECT id FROM posts WHERE post_type_id = :post_type_id ORDER BY name ASC", [
                    'post_type_id' => $post_type->id
                ] );

            else :

                $page = $per_page > 0 && $page == 0 ? 1 : $page;
                $per_page = $page > 0 && $per_page == 0 ? 15 : $per_page;

                if ( !empty( $forms ) && $per_page == 0 && $page == 0 ) :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND posts.parent_id IN ( " . implode( ', ', $forms ) . " ) AND post_meta.meta_key = :order ORDER BY post_meta.meta_value ASC", [
                            'post_type_id' => $post_type->id,
                            'order' => 'order'
                        ]
                    );

                elseif ( empty( $forms ) && $per_page > 0 ) :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND post_meta.meta_key = :order ORDER BY post_meta.meta_value ASC LIMIT :lower_limit, :per_page", [
                            'post_type_id' => $post_type->id,
                            'order' => 'order',
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                else :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND posts.parent_id IN ( " . implode( ', ', $forms ) . " ) AND post_meta.meta_key = :order ORDER BY post_meta.meta_value ASC LIMIT :lower_limit, :per_page", [
                            'post_type_id' => $post_type->id,
                            'order' => 'order',
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                endif;

            endif;

            foreach( $records as $record )
                $form_sections[] = new FormSection( $record->id );

        endif;

        return isset( $form_sections ) ? $form_sections : [];
    }

}