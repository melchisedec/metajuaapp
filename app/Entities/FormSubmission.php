<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

class FormSubmission {

	public $id;
	public $name;
	public $form;
    public $user;
    public $timestamp;

	public function __construct( $identifier ) {
		$post = new Post( 'Form submission', $identifier );

		if ( $post->id != null ) :

			$this->id = $post->id;
			$this->name = $post->name;
            $this->form = $post->parent_id;
            $this->user = $post->user_id;

            $meta = new PostMeta( $this->id, 'timestamp' );
            $this->timestamp = $meta->meta_value;

		endif;
	}

	public static function add( $form_id = null, $name = '', $timestamp = null, $user_id = null ) {
		$post_id = Post::add( 'Form submission', $name, $form_id, $user_id );

		$timestamp = $timestamp == null ? date( 'Y-m-d h:i:s' ) : $timestamp;

        PostMeta::add( $post_id, 'timestamp', $timestamp );

        return $post_id;
	}

    public static function get_form_submissions( $forms = [], $per_page = 0, $page = 0 ) {
	    $post_type = new PostType( 'Form submission' );

	    if ( $post_type->id != null ) :

            if ( empty( $forms ) && $per_page == 0 && $page == 0 ) :

                $records = DB::select( "SELECT id FROM posts WHERE post_type_id = :post_type_id ORDER BY name ASC", [
                    'post_type_id' => $post_type->id
                ] );

            else :

                $page = $per_page > 0 && $page == 0 ? 1 : $page;
                $per_page = $page > 0 && $per_page == 0 ? 15 : $per_page;

                if ( !empty( $forms ) && $per_page == 0 && $page == 0 ) :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND posts.parent_id IN ( " . implode( ', ', $forms ) . " ) AND post_meta.meta_key = :timestamp ORDER BY posts.name ASC", [
                            'post_type_id' => $post_type->id,
                            'timestamp' => 'timestamp'
                        ]
                    );

                elseif ( empty( $forms ) && $per_page > 0 ) :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND post_meta.meta_key = :timestamp ORDER BY posts.name ASC LIMIT :lower_limit, :per_page", [
                            'post_type_id' => $post_type->id,
                            'timestamp' => 'timestamp',
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                else :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM post_meta LEFT JOIN posts ON post_meta.post_id = posts.id WHERE posts.post_type_id = :post_type_id AND posts.parent_id IN ( " . implode( ', ', $forms ) . " ) AND post_meta.meta_key = :timestamp ORDER BY posts.name ASC LIMIT :lower_limit, :per_page", [
                            'post_type_id' => $post_type->id,
                            'timestamp' => 'timestamp',
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                endif;

            endif;

            foreach( $records as $record )
                $form_submissions[] = new FormSubmission( $record->id );

        endif;

        return isset( $form_submissions ) ? $form_submissions : [];
    }

}