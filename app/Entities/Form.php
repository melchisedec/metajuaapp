<?php

namespace App\Entities;

use Illuminate\Support\Facades\DB;

class Form {

	public $id;
	public $name;
	public $module;

	public function __construct( $identifier ) {
		$post = new Post( 'Form', $identifier );

		if ( $post->id != null ) :

			$this->id = $post->id;
			$this->name = $post->name;

			$meta = new PostMeta( $this->id, 'module' );
			$this->module = ( int ) $meta->meta_value;

		endif;
	}

	public static function add( $name = '', $user_id = null ) {
		if ( empty( $name ) )
			return null;

		return Post::add( 'Form', $name, $user_id );
	}

    public static function get_forms( $modules = [], $per_page = 0, $page = 0 ) {
	    $post_type = new PostType( 'Form' );

	    if ( $post_type->id != null ) :

            if ( empty( $modules ) && $per_page == 0 && $page == 0 ) :

                $records = DB::select( "SELECT id FROM posts WHERE post_type_id = :post_type_id ORDER BY name ASC", [
                    'post_type_id' => $post_type->id
                ] );

            else :

                $page = $per_page > 0 && $page == 0 ? 1 : $page;
                $per_page = $page > 0 && $per_page == 0 ? 15 : $per_page;

                if ( !empty( $modules ) && $per_page == 0 && $page == 0 ) :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM posts LEFT JOIN post_meta ON posts.id = post_meta.post_id WHERE posts.post_type_id = :post_type_id AND post_meta.meta_key = :module AND post_meta.meta_value IN ( " . implode( ', ', $modules ) . " ) ORDER BY posts.name ASC", [
                            'post_type_id' => $post_type->id,
                            'module' => 'module'
                        ]
                    );

                elseif ( empty( $modules ) && $per_page > 0 ) :

                    $records = DB::select(
                        "SELECT id FROM posts WHERE post_type_id = :post_type_id ORDER BY name ASC LIMIT :lower_limit, :per_page", [
                            'post_type_id' => $post_type->id,
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                else :

                    $records = DB::select(
                        "SELECT posts.id AS id FROM posts LEFT JOIN post_meta ON posts.id = post_meta.post_id WHERE posts.post_type_id = :post_type_id AND post_meta.meta_key = :module AND post_meta.meta_value IN ( " . implode( ', ', $modules ) . " ) ORDER BY posts.name ASC LIMIT :lower_limit, :per_page", [
                            'post_type_id' => $post_type->id,
                            'module' => 'module',
                            'lower_limit' => ( $page - 1 ) * $per_page,
                            'per_page' => $per_page
                        ]
                    );

                endif;

            endif;

            foreach( $records as $record )
                $forms[] = new Form( $record->id );

        endif;

        return isset( $forms ) ? $forms : [];
    }

}