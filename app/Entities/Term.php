<?php

namespace App\Entities;

use App\Models\Terms;

class Term {

	public $id;
	public $taxonomy_id;
	public $user_id;
	public $name;
	public $meta = [];

	public function __construct( $taxonomy, $identifier, $user_id = null ) {
		$taxonomy = new Taxonomy( $taxonomy );

		if ( $taxonomy->id != null ) :

			$query = Terms::where( 'taxonomy_id', '=', $taxonomy->id );

			is_numeric( $identifier ) ?
				$query->where( 'id', '=', $identifier ) :
				$query->where( 'name', 'LIKE', strtolower( $identifier ) );

			$user_id != null ? $query->where( 'user_id', '=', $user_id ) : null;

			$record = $query->first();

			if ( !empty( $record ) ) :

				$this->id = $record->id;
				$this->taxonomy_id = $record->taxonomy_id;
				$this->user_id = $record->user_id;
				$this->name = $record->name;

			endif;

		endif;
	}

	public static function add( $taxonomy, $name, $user_id = null ) {
		if ( empty( $name ) )
			return null;

		$taxonomy = new Taxonomy( $taxonomy );

		if ( $taxonomy->id == null )
			return null;

		$term = new Term( $taxonomy->id, $name, $user_id );

		if ( $term->id != null )
			return $term->id;

		$term = new Terms();
		$term->taxonomy_id = $taxonomy->id;
		$user_id != null ? $term->user_id = $user_id : null;
		$term->name = $name;
		$term->save();

		return $term->id;
	}

}