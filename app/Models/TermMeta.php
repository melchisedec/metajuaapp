<?php
/**
 * Created by PhpStorm.
 * User: King
 * Date: 14/04/2017
 * Time: 15:23
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TermMeta extends Model {

    protected $table = 'term_meta';
    protected $primaryKey = 'id';
    protected $fillable = [ 'term_id', 'meta_key', 'meta_value' ];
    protected $hidden = [ 'created_at', 'updated_at' ];

}