<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model {

    protected $table = 'user_meta';
    protected $primaryKey = 'id';
    protected $fillable = [ 'meta_key','user_id', 'meta_value' ];
    protected $hidden = [ 'created_at', 'updated_at' ];

}
