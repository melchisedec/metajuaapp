<?php
/**
 * Created by PhpStorm.
 * User: King
 * Date: 14/04/2017
 * Time: 15:14
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model {

    protected $table = 'posts';
    protected $primaryKey = 'id';
    protected $fillable = [ 'post_type_id', 'user_id', 'name', 'parent_id' ];
    protected $hidden = [ 'parent_id', 'auto_load', 'created_at', 'updated_at' ];

}