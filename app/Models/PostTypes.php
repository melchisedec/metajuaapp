<?php
/**
 * Created by PhpStorm.
 * User: King
 * Date: 14/04/2017
 * Time: 15:17
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostTypes extends Model {

    protected $table = 'post_types';
    protected $primaryKey = 'id';
    protected $fillable = [ 'singular', 'plural' ];
    protected $hidden = [ 'created_at', 'updated_at' ];

}