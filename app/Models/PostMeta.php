<?php
/**
 * Created by PhpStorm.
 * User: King
 * Date: 14/04/2017
 * Time: 15:19
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PostMeta extends Model {

    protected $table = 'post_meta';
    protected $primaryKey = 'id';
    protected $fillable = [ 'post_id', 'meta_key', 'meta_value' ];
    protected $hidden = [ 'created_at', 'updated_at' ];

}