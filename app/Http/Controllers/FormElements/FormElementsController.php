<?php

namespace App\Http\Controllers\FormElements;

use Illuminate\Support\Facades\Input;

use App\Entities\FormElement;
use App\Entities\PostMeta;


use App\Http\Controllers\Controller;

class FormElementsController extends Controller {

    public function index() {
        $form = ( array ) Input::get( 'form' );
        $per_page = ( int ) Input::get( 'per_page', 1 );
        $page = ( int ) Input::get( 'page', 1 );

        foreach( FormElement::get_form_elements( $form, $per_page, $page ) as $form_element ) :

            $form_elements[] = [
                'id' => $form_element->id,
                'name' => $form_element->name,
                'form' => $form_element->form,
                'meta_data' =>  PostMeta::get_post_meta( $form_element->id )
            ];

        endforeach;

		return response()->json( [
		    'result' => 'successful',
            'form_elements' => !empty( $form_elements ) ?  $form_elements : []
        ] );
    }

}