<?php

namespace App\Http\Controllers\Modules;

use App\Entities\Module;
use App\Entities\PostType;
use App\Entities\Form;

use App\Http\Controllers\Controller;

class ModuleController extends Controller {

    public function single( $module ) {
        $module = new Module( $module );

        return response()->json( [
            'result' => $module->id == null ? 'failed' : 'successful',
            'module' => $module
        ] );
    }

    public function forms( $module ) {
        $module = new Module( $module );

        if ( $module->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Invalid module passed'
            ] );

        $post_type = new PostType( 'Form' );

        if ( $post_type->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Error occured. Contact administrator'
            ] );

        return response()->json( [
            'result' => 'successful',
            'post_type' => $post_type,
            'forms' => Form::get_forms( [ $module->id ] )
        ] );
    }

}