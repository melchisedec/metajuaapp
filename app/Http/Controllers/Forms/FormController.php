<?php

namespace App\Http\Controllers\Forms;

use Illuminate\Support\Facades\Input;

use App\Entities\Form;
use App\Entities\PostType;
use App\Entities\FormSection;
use App\Entities\FormSubmission;

use App\Http\Controllers\Controller;

class FormController extends Controller {

    public function single( $form ) {
        $form = new Form( $form );

        return response()->json( [
            'result' => $form->id == null ? 'failed' : 'successful',
            'module' => $form
        ] );
    }

    public function sections( $form ) {
        $form = new Form( $form );

        if ( $form->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Invalid form passed'
            ] );

        $post_type = new PostType( 'Form section' );

        if ( $post_type->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Error occured. Contact administrator'
            ] );

        $per_page = ( int ) Input::get( 'per_page', 15 );
        $page = ( int ) Input::get( 'page', 1 );

        return response()->json( [
            'result' => 'successful',
            'post_type' => $post_type,
            'form_sections' => FormSection::get_form_sections( [ $form->id ], $per_page, $page )
        ] );
    }

    public function submissions( $form ) {
        $form = new Form( $form );

        if ( $form->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Invalid form passed'
            ] );

        $post_type = new PostType( 'Form submission' );
        $response_post_type = new PostType( 'Form element response' );

        if ( $post_type->id == null || $response_post_type->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Error occured. Contact administrator'
            ] );

        $per_page = ( int ) Input::get( 'per_page', 15 );
        $page = ( int ) Input::get( 'page', 1 );

        return response()->json( [
            'result' => 'successful',
            'post_types' => [
                'submission' => $post_type,
                'response' => $response_post_type
            ],
            'form_submissions' => FormSubmission::get_form_submissions( [ $form->id ], $per_page, $page )
        ] );
    }

    public function add_submission( $form ) {
        $form = new Form( $form );

        if ( $form->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Invalid form passed'
            ] );

        $user = Input::get( 'user', null );
        $timestamp = Input::get( 'timestamp', date( 'Y-m-d h:i:s' ) );

        $current_submissions = FormSubmission::get_form_submissions( [ $form->id ] );

        if ( count( $current_submissions ) > 0 ) :

            $recent = $current_submissions[ count( $current_submissions ) - 1 ];

            $chunks = explode( '/', $recent->name );

            $next = ( ( ( int ) end( $chunks ) ) + 1 );

        else :

            $next = 1;

        endif;

        $name = 'SUB/' . str_pad( $form->id, 4, '0', STR_PAD_LEFT ) . '/' . date( 'y' ) . '/' . str_pad( $next, 6, '0', STR_PAD_LEFT  );

        $submission = new FormSubmission( FormSubmission::add( $form->id, $name, $timestamp, $user ) );

        if ( $submission->id == null )
            return response()->json( [
                'result' => 'failed',
                'message' => 'Failed to add submission'
            ] );

        return response()->json( [
            'result' => 'successful',
            'data' => [
                'id' => $submission->id,
                'name' => $submission->name
            ]
        ] );
    }

}