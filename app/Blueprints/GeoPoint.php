<?php

namespace App\Blueprints;

class GeoPoint extends Blueprint {

    public $attributes = [ 'label', 'placeholder', 'required', 'accuracy', 'include_altitude', 'button_text', 'hidden'  ];
    public $required = [ 'label' ];
    public $defaults = [
        'accuracy' => 0.2,
        'include_attitude' => false,
        'button_text' => 'Read GPS',
        'required' => false,
        'hidden' => false
    ];

    public static function get_defaults() {
        $blueprint = new GeoPoint();

        return $blueprint->defaults;
    }

}