<?php

namespace App\Blueprints;

class TextArea {

    public $attributes = [ 'label', 'placeholder', 'required', 'options', 'default', 'allow_null', 'read_only', 'value', 'hidden'  ];
    public $required = [ 'label' ];
    public $defaults = [
        'required' => false,
        'hidden' => false
    ];

    public static function get_defaults() {
        $blueprint = new TextArea();

        return $blueprint->defaults;
    }

}