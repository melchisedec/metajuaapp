<?php

namespace App\Blueprints;

class Checkbox  extends Blueprint {

    public $attributes = [ 'label', 'required', 'options', 'default', 'allow_null', 'dynamic_options', 'hidden' ];
    public $required = [ 'label' ];
    public $defaults = [
        'dynamic_options' => false,
        'options' => [],
        'query' => [],
        'required' => false,
        'hidden' => false,
        'allow_null' => true
    ];

    public static function get_defaults() {
        $blueprint = new Checkbox();

        return $blueprint->defaults;
    }

}