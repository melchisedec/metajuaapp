<?php

namespace App\Blueprints;

class Blueprint {

    public $attributes = [];
    public $required = [];
    public $defaults = [];

    public static function validate( $params = [] ) {
        
        $blueprint = new Blueprint();

        foreach( $blueprint->required as $field )

        if ( !in_array( $field, array_keys( $params ) ) )
            return false;

        return true;
    }

    public static function get_defaults() {
        $blueprint = new Blueprint();

        return $blueprint->defaults;
    }
    
}