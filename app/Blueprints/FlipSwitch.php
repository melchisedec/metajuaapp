<?php

namespace App\Blueprints;

class FlipSwitch extends Blueprint {

    public $attributes = [ 'label',  'required', 'read_only', 'on_value', 'off_value', 'hidden' ];
    public $required = [ 'label' ];
    public $defaults = [
        'required' => false,
        'hidden' => false,
        'on_value' => 'yes',
        'off_value' => 'no'
    ];

    public static function get_defaults() {
        $blueprint = new FlipSwitch();

        return $blueprint->defaults;
    }

}