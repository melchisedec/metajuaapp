<?php

namespace App\Blueprints;

class Date  extends Blueprint {

    public $attributes = [ 'label', 'required', 'default', 'value', 'format', 'hidden', 'use_current_date', 'min', 'max'  ];
    public $required = [ 'label' ];
    public $defaults = [
        'required' => false,
        'hidden' => false,
        'use_current_date' => false
    ];

    public static function get_defaults() {
        $blueprint = new Date();

        return $blueprint->defaults;
    }
}