<?php

namespace App\Blueprints;

class Time  extends Blueprint {

    public $attributes = [ 'label', 'required',  'default', 'value', 'format', 'hidden', 'use_current_time' ];
    public $required = [ 'label' ];
    public $defaults = [
        'required' => false,
        'hidden' => false,
        'use_current_time' => false
    ];

    public static function get_defaults() {
        $blueprint = new Time();

        return $blueprint->defaults;
    }

}