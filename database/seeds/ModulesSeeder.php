<?php

use Illuminate\Database\Seeder;

use App\Entities\Taxonomy;
use App\Entities\Module;
use App\Entities\Account;
use App\Entities\TermMeta;

class ModulesSeeder extends Seeder  {

    public function run() {
	    Taxonomy::add( 'Module', 'Modules' );

        $metajua = new Account( 'Metajua' );

        $entries = [
            [
                'name' => 'Supply Chain Management',
                'meta_data' => [
                    'account' => $metajua->id,
                    'icon' => 'layers_fill',
                    'order' => 1
                ]
            ],
            [
                'name' => 'Farmer Registration',
                'meta_data' => [
                    'account' => $metajua->id,
                    'icon' => 'persons_fill',
                    'order' => 2
                ]
            ],
            [
                'name' => 'Statistics',
                'meta_data' => [
                    'account' => $metajua->id,
                    'icon' => 'graph_round_fill',
                    'order' => 3
                ]
            ]
        ];

        foreach( $entries as $entry ) :

            $entry = ( object ) $entry;

            $module = new Module( Module::add( $entry->name ) );

            foreach( $entry->meta_data as $meta_key => $meta_value )
                TermMeta::add( $module->id, $meta_key, $meta_value );

        endforeach;
    }

}
