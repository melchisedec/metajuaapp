<?php

use Illuminate\Database\Seeder;

use App\Entities\PostType;
use App\Entities\Module;
use App\Entities\Form;
use App\Entities\PostMeta;
use App\Entities\FormSection;
use App\Entities\FormElement;

class FormsSeeder extends Seeder  {

    private function seed_forms() {
        PostType::add( 'Form', 'Forms' );
        PostType::add( 'Form section', 'Forms sections' );
        PostType::add( 'Form element', 'Forms elements' );
        PostType::add( 'Form submission', 'Form submissions' );
        PostType::add( 'Form element response', 'Form element response' );

        $scm_module = new Module( 'Supply Chain Management' );
        $fr_module = new Module( 'Farmer Registration' );

        $entries = [
            [
                'name' => 'Short organizational mapping',
                'module' => $scm_module->id,
                'sections' => [
                    [
                        'name' => 'Location',
                        'order' => 1,
                        'elements' =>  [
                            [
                                'name' => 'Region',
                                'type' => 'text',
                                'meta_data' => [
                                    'label' => 'Region',
                                    'error_message' => 'Region is a required field',
                                    'required' => true,
                                    'order' => 1
                                ]
                            ],
                            [
                                'name' => 'Sub-county',
                                'type' => 'text',
                                'meta_data' => [
                                    'label' => 'Sub-county',
                                    'error_message' => 'Sub-county is a required field',
                                    'required' => true,
                                    'order' => 3
                                ]
                            ],
                            [
                                'name' => 'District',
                                'type' => 'text',
                                'meta_data' => [
                                    'label' => 'District',
                                    'error_message' => 'District is a required field',
                                    'required' => true,
                                    'order' => 2
                                ]
                            ],
                            [
                                'name' => 'Village',
                                'type' => 'text',
                                'meta_data' => [
                                    'label' => 'Village',
                                    'error_message' => 'Village is a required field',
                                    'required' => true,
                                    'order' => 4,
                                ]
                            ],
                            [
                                'name' => 'Read GPS',
                                'type' => 'geo-point',
                                'meta_data' => [
                                    'label' => 'GPS coordinates',
                                    'error_message' => 'GPS cordinates are required',
                                    'required' => true,
                                    'order' => 5
                                ]
                            ]
                        ]
                    ]
                ]
            ],

            [
                'name' => 'Extensive farmer registration',
                'module' => $fr_module->id,
                'sections' =>[
                    [
                        'name'=> 'Introduction',
                        'order' => 1,
                        'hidden' => true,
                        'elements' =>[
                            [
                                'name'=>'Date of survey',
                                'type' => 'date',
                                'meta_data' => [
                                    'label'=> 'Date of survey',
                                    'required' => true,
                                    'order' => 1,
                                    'hidden' => true
                                ]
                            ],
                            [
                                'name'=>'Start time',
                                'type' => 'time',
                                'meta_data' => [
                                    'label'=> 'Start time',
                                    'required' => true,
                                    'order' => 2,
                                    'hidden' => true
                                ]
                            ],
                            [
                                'name'=>'Farmer ID',
                                'type' => 'text',
                                'meta_data' => [
                                    'label'=> 'Farmer ID',
                                    'readonly' => true,
                                    'required' => true,
                                    'order' => 3,
                                    'hidden' => true
                                ]
                            ]
                        ]
                    ],
                    [
                        'name'=> 'Bio-data',
                        'order' => 2,
                        'elements' =>[
                            [
                                'name'=>'Farmer Name',
                                'type' => 'text',
                                'meta_data' => [
                                    'label'=> 'Name of farmer',
                                    'required' => true,
                                    'order' => 1,
                                    'hidden' => false
                                ]
                            ],
                            [
                                'name'=>'Gender',
                                'type' => 'select',
                                'meta_data' => [
                                    'label'=> 'Gender',
                                    'required' => true,
                                    'order' => 2,
                                    'hidden' => false,
                                    'options' => [ 'Male','Female' ]
                                ]
                            ],
                            [
                                'name'=>'Date of birth',
                                'type' => 'date',
                                'meta_data' => [
                                    'label'=> 'Date of birth',
                                    'required' => true,
                                    'order' => 3,
                                    'hidden' => false
                                ]
                            ],
                            [
                                'name'=>'Do you have a phone',
                                'type' => 'flip-switch',
                                'meta_data' => [
                                    'label'=> 'Do you have a phone',
                                    'required' => true,
                                    'order' => 4,
                                    'hidden' => false,
                                    'off_value' => 'no',
                                    'on_value' => 'yes'
                                ]
                            ],
                            [
                                'name'=>'Network provider',
                                'type' => 'select',
                                'meta_data' => [
                                    'label'=> 'Network provider',
                                    'required' => true,
                                    'order' => 5,
                                    'hidden' => false,
                                    'options' => [ 'MTN','Africell', 'Airtel' ]
                                ]
                            ],
                            [
                                'name'=>'Telephone number',
                                'type' => 'number',
                                'meta_data' => [
                                    'label'=> 'Telephone number',
                                    'required' => true,
                                    'order' => 6,
                                    'hidden' => false

                                ]
                            ],
                            [
                                'name'=>'Type of ID',
                                'type' => 'select',
                                'meta_data' => [
                                    'label'=> 'Type of ID',
                                    'required' => true,
                                    'order' => 7,
                                    'hidden' => false,
                                    'options' => [ 'National ID','Passport', 'Driving permit','Others' ]
                                ]
                            ],
                            [
                                'name'=>'ID No',
                                'type' => 'text',
                                'meta_data' => [
                                    'label'=> 'ID No',
                                    'required' => true,
                                    'order' => 8,
                                    'hidden' => false
                                ]
                            ]
                        ]
                    ],
                    [
                        'name'=> 'Location',
                        'order' => 3,
                        'elements' =>[
                            [
                                'name'=>'Region',
                                'type' => 'select',
                                'meta_data' => [
                                    'label'=> 'Region',
                                    'required' => true,
                                    'order' => 1,
                                    'hidden' => false,
                                    'options' => [ 'Central region','Western region' ]
                                ]
                            ],
                            [
                                'name'=>'District',
                                'type' => 'select',
                                'meta_data' => [
                                    'label'=> 'District',
                                    'required' => true,
                                    'order' => 2,
                                    'hidden' => false,
                                    'options' => [ 'Wakiso','Entebbe' ]
                                ]
                            ],
                            [
                                'name'=>'Subcounty',
                                'type' => 'select',
                                'meta_data' => [
                                    'label'=> 'Subcounty',
                                    'required' => true,
                                    'order' => 3,
                                    'hidden' => false,
                                    'options' => [ 'Kyadondo','Kira' ]
                                ]
                            ],
                            [
                                'name'=>'Village',
                                'type' => 'select',
                                'meta_data' => [
                                    'label'=> 'Village',
                                    'required' => true,
                                    'order' => 4,
                                    'hidden' => false,
                                    'options' => [ 'Village 1', 'Village 2' ]
                                ]
                            ],
                            [
                                'name' => 'Favourite fruits',
                                'type' => 'checkbox',
                                'meta_data' => [
                                    'required' => false,
                                    'label' => 'Choose your favourite fruits',
                                    'order' => 5,
                                    'options' => [
                                        'apples' => 'Apples',
                                        'mangoes' => 'Mangoes',
                                        'oranges' => 'Oranges',
                                        'pawpaws' => 'Pawpaws',
                                        'watermelon' => 'Watermelons'
                                    ]
                                ]
                            ],
                        ]
                    ],
                    [
                        'name'=> 'HH-data',
                        'order' => 4,
                        'elements' =>[
                            [
                                'name'=>'How many members compose your HH',
                                'type' => 'number',
                                'meta_data' => [
                                    'label'=> 'How many members compose your HH',
                                    'required' => true,
                                    'order' => 1,
                                    'hidden' => false
                                ]
                            ],
                            [
                                'name'=>'HH member name',
                                'type' => 'text',
                                'meta_data' => [
                                    'label'=> 'HH member name',
                                    'required' => true,
                                    'order' => 2,
                                    'hidden' => false
                                ]
                            ],
                            [
                                'name'=>'Position',
                                'type' => 'select',
                                'meta_data' => [
                                    'label'=> 'Position',
                                    'required' => true,
                                    'order' => 3,
                                    'hidden' => false,
                                    'options' => [ 'Husband','Wife','Son','Daughter', 'Other' ]
                                ]
                            ],
                            [
                                'name'=>'Is HH member actively involved on the farm',
                                'type' => 'flipswitch',
                                'meta_data' => [
                                    'label'=> 'Is HH member actively involved on the farm',
                                    'required' => true,
                                    'order' => 4,
                                    'off_value' => 'no',
                                    'on_value' => 'yes',
                                    'hidden' => false

                                ]
                            ]
                        ]
                    ],
                    [
                        'name'=> 'Farmer Details',
                        'order' => 5,
                        'elements' =>[
                            [
                                'name' => 'Farmer photo',
                                'type' => 'photo',
                                'meta_data' => [
                                    'label' => 'Attach a photo',
                                    'file_ext' => 'jpg',
                                    'min' => 0,
                                    'max' => 18,
                                    'width' => 250,
                                    'height' => 250,
                                    'allow_null' => true,
                                    'order' => 1
                                ]
                            ],
                            [
                                'name'=>'GPS',
                                'type' => 'geo-point',
                                'meta_data' => [
                                    'label'=> 'Read HH-GPS',
                                    'required' => true,
                                    'order' => 2,
                                    'hidden' => false
                                ]
                            ],
                            [
                                'name'=>'Endtime',
                                'type' => 'time',
                                'meta_data' => [
                                    'label'=> 'Endtime',
                                    'required' => true,
                                    'order' => 3,
                                    'hidden' => true
                                ]
                            ]
                        ]
                    ]

                ]
            ],

            [
                'name' => 'Farm plot registration',
                'module' =>  $fr_module->id,
                'sections' => [
                    [
                        'name' => 'Introduction',
                        'order' => 1,
                        'elements' => [
                            [
                                'name' => 'Date',
                                'type' => 'date',
                                'meta_data' => [
                                    'label' => 'Date of survey',
                                    'required' => true,
                                    'order' => 1,
                                    'use_current_date' => true
                                ]
                            ],
                            [
                                'name' => 'Time',
                                'type' => 'time',
                                'meta_data' => [
                                    'label' => 'Start time',
                                    'required' => true,
                                    'order' => 2,
                                    'use_current_time' => true
                                ]
                            ],
                            [
                                'name' => 'Farmer ID',
                                'type' => 'text',
                                'meta_data' => [
                                    'label' => 'Farmer ID',
                                    'required' => true,
                                    'order' => 2,
                                ]
                            ]
                        ]
                    ],
                    [
                        'name' => 'Plot',
                        'order' => 3,
                        'elements' => [
                            [
                                'name' => 'Farmer',
                                'type' => 'text',
                                'meta_data' => [
                                    'label' => 'Recall farmer ID',
                                    'required' => true,
                                    'order' => 1,
                                    'hidden' => true
                                ]
                            ],
                            [
                                'name' => 'Assign plot ID',
                                'type' => 'select',
                                'meta_data' => [
                                    'label' => 'Assign plot ID',
                                    'order' => 2,
                                    'hidden' => true
                                ]
                            ],
                            [
                                'name' => 'Plot name',
                                'type' => 'text',
                                'meta_data' => [
                                    'label' => 'Plot name',
                                    'order' => 3
                                ]
                            ],
                            [
                                'name' => 'Plot acreage',
                                'type' => 'number',
                                'meta_data' => [
                                    'label' => 'Plot acreage',
                                    'order' => 4
                                ]
                            ],
                        ]
                    ],
                    [
                        'name' => 'Coffee',
                        'order' => 4,
                        'elements' => [
                            [
                                'name' => 'Coffee acreage',
                                'type' => 'number',
                                'meta_data' =>[
                                    'label' => 'Coffee acreage',
                                    'order' => 1
                                ]
                            ],
                            [
                                'name' => 'What coffee type',
                                'type' => 'select',
                                'meta_data' => [
                                    'label' => 'What coffee type ?',
                                    'options' => [
                                        'Arabica',
                                        'Robusta',
                                        'Not applicable'
                                    ],
                                    'order' => 2
                                ]
                            ],
                            [
                                'name' => 'Ages of coffee plants',
                                'type' => 'multiple-select',
                                'meta_data' => [
                                    'label' => 'Ages of coffee plants ?',
                                    'options' => [
                                        '0 - 1 years',
                                        '1 - 4 years',
                                        '4 - 10 years',
                                        'Over 10 years'
                                    ],
                                    'required' => true,
                                    'order' => 3
                                ]
                            ],
                        ]
                    ],
                    [
                        'name' => 'Harvests',
                        'order' => 5,
                        'elements' => [
                            [
                                'name' => 'Coffee harvested',
                                'type' => 'select',
                                'meta_data' => [
                                    'label' => 'How much coffee was harvested',
                                    'options' => [
                                        'Farmer know harvest in kgs',
                                        'Estimate the number of kg'
                                    ],
                                    'order' => 1
                                ]
                            ],
                            [
                                'name' => 'Quantity of cherries',
                                'type' => 'number',
                                'meta_data' => [
                                    'label' => 'How many kgs of cherries were harvested',
                                    'order' => 2
                                ]
                            ],
                            [
                                'name' => 'Select the measure',
                                'type' => 'select',
                                'meta_data' => [
                                    'label' => 'Select the measure',
                                    'options' => [
                                        'Cups',
                                        'Basins',
                                        '20L Plastic jerricans',
                                        'Bag(s)'
                                    ],
                                    'order' => 3
                                ]
                            ],
                            [
                                'name' => 'How many units were harvested',
                                'type' => 'number',
                                'meta_data' => [
                                    'label' => 'Units of harvest measure',
                                    'order' => 13
                                ]
                            ],
                        ]
                    ],
                    [
                        'name' => 'Sales',
                        'order' => 6,
                        'elements' => [
                            [
                                'name' => 'Did you sell the coffee produced as',
                                'type' => 'select',
                                'meta_data' => [
                                    'label' => 'Produce was sold as',
                                    'options' => [
                                        'Cherry',
                                        'Kiboko',
                                        'Parchment',
                                        'FAQ'
                                    ],
                                    'order' => 14
                                ]
                            ]
                        ]
                    ]
                    /*,




                    [
                        'name' => 'Did you sell the coffee produced as',
                        'type' => 'select',
                        'meta_data' => [
                            'label' => 'Produce was sold as',
                            'options' => [
                                'Cherry',
                                'Kiboko',
                                'Parchment',
                                'FAQ'
                            ],
                            'order' => 14
                        ]
                    ],
                    [
                        'name' => 'Does the farmer know how much {Cherry/Kiboko/Parchment/FAQ} sold in kgs',
                        'type' => 'select',
                        'meta_data' => [
                            'label' =>'Does the farmer know how much {Cherry/Kiboko/Parchment/FAQ} sold in kgs ?',
                            'options' => ['Yes','No'],
                            'order' => 15
                        ]
                    ],
                    [
                        'name' => 'How many kgs was sold',
                        'type' => 'number',
                        'meta_data' => [
                            'label' => 'How many kgs was sold ?',
                            'order' => 16
                        ]
                    ],
                    [
                        'name' => 'Select the measure',
                        'type' => 'select',
                        'meta_data' => [
                            'label' => 'Select the measure ?',
                            'options' => [ 'Cups','Basins','20L Plastic Jerrycans','Bag(s)' ],
                            'order' => 17
                        ]
                    ],
                    [
                        'name' => 'How many UNITS for the selected measure was sold',
                        'type' => 'number',
                        'meta_data' => [
                            'label' => 'How many UNITS for the selected measure was sold ?',
                            'order' => 18
                        ]
                    ],
                    [
                        'name' => 'Do you have another plot',
                        'type' => 'select',
                        'meta_data' => [
                            'label' => 'Do you have another plot ?',
                            'options' => ['Yes','No'],
                            'order' => 19
                        ]
                    ]
                    */
                ]
            ],
            /*[
                'name' => 'Extensive Farmer Registration',
                'module' => $fr_module->id,
                'elements' => [
                    [
                        'name' => 'Date',
                        'type' => 'date',
                        'meta_data' => [
                            'label' => 'Date of survey',
                            'required' => true,
                            'order' => 1,
                            'hidden' => true,
                            'use_current_date' => true
                        ]
                    ],
                    [
                        'name' => 'Start time',
                        'type' => 'time',
                        'meta_data' => [
                            'label' => 'Start time',
                            'order' => 2,
                            'hidden' => true,
                            'use_current_time' => true
                        ]
                    ],
                    [
                        'name' => 'Farmer code',
                        'type' => 'text',
                        'meta_data' => [
                            'label' => 'Assign farmer code',
                            'required' => true,
                            'order' => 3,
                            'hidden' => true
                        ]
                    ],
                    [
                        'name' => 'Farmer name',
                        'type' => 'text',
                        'meta_data' => [
                            'label' => 'Name of the farmer',
                            'required' => true,
                            'order' => 4
                        ]
                    ],
                    [
                        'name' => 'Sex',
                        'type' => 'select',
                        'meta_data' => [
                            'required' => true,
                            'label' => 'Sex',
                            'options' => [ 'Male', 'Female' ],
                            'order' => 5
                        ]
                    ],
                    [
                        'name' => 'Date Of birth',
                        'type' => 'date',
                        'meta_data' => [
                            'required' => true,
                            'label' => 'Date of birth',
                            'order' => 6
                        ]
                    ],
                    [
                        'name' => 'Favourite fruits',
                        'type' => 'checkbox',
                        'meta_data' => [
                            'required' => false,
                            'label' => 'Choose your favourite fruits',
                            'order' => 7,
                            'options' => [
                                'apples' => 'Apples',
                                'mangoes' => 'Mangoes',
                                'oranges' => 'Oranges',
                                'pawpaws' => 'Pawpaws',
                                'watermelon' => 'Watermelons'
                            ]
                        ]
                    ],
                    [
                        'name' => 'Do you have a phone',
                        'type' => 'flip-switch',
                        'meta_data' => [
                            'label' => 'Do you have a phone?',
                            'required' => true,
                            'off_value' => 'no',
                            'on_value' => 'yes',
                            'order' => 8
                        ]
                    ],
                    [
                        'name' => 'Network provider',
                        'type' => 'select',
                        'meta_data' => [
                            'label' => 'Network provider',
                            'required' => true,
                            'options' => [ 'MTN', 'Airtel', 'Africel' ],
                            'order' => 9
                        ]
                    ],
                    [
                        'name' => 'Farmer photo',
                        'type' => 'photo',
                        'meta_data' => [
                            'label' => 'Attach a photo',
                            'file_ext' => 'jpg',
                            'min' => 0,
                            'max' => 18,
                            'width' => 250,
                            'height' => 250,
                            'allow_null' => true,
                            'order' => 10
                        ]
                    ],
                    [
                        'name' => 'Telephone number',
                        'type' => 'number',
                        'meta_data' => [
                            'label' => 'Telephone number',
                            'required' => true,
                            'max' => 10,
                            'min' => 10,
                            'order' => 11
                        ]
                    ],
                    [
                        'name' => 'Type of ID',
                        'type' => 'select',
                        'meta_data' => [
                            'label' => 'Type of ID',
                            'required' => true,
                            'options' => [ 'National ID', 'Passport', 'Driving permit', 'Others' ],
                            'order' => 12
                        ]
                    ],
                    [
                        'name' => 'ID number',
                        'type' => 'text',
                        'meta_data' => [
                            'label' => 'ID number',
                            'required' => true ,
                            'order' => 13
                        ]
                    ],
                    [
                        'name' => 'Region',
                        'type' => 'select',
                        'meta_data' => [
                            'label' => 'Region',
                            'error_message' => 'Region is a required field',
                            'required' => true,
                            'order' => 14
                        ]
                    ],
                    [
                        'name' => 'Sub-county',
                        'type' => 'select',
                        'meta_data' => [
                            'label' => 'Sub-county',
                            'order' => 15
                        ]
                    ],
                    [
                        'name' => 'Village',
                        'type' => 'select',
                        'meta_data' => [
                            'label' => 'Village',
                            'required' => false,
                            'order' => 16
                        ]
                    ],
                    [
                        'name' => 'District',
                        'type' => 'select',
                        'meta_data' => [
                            'label' => 'District',
                            'required' => true,
                            'order' => 17
                        ]
                    ],
                    [
                        'name' => 'How many members compose your HH',
                        'type' => 'number',
                        'meta_data' => [
                            'label' => 'How many members compose your HH?',
                            'required' => true,
                            'order' => 18
                        ]
                    ],
                    
                    [
                        'name' => 'Read GPS',
                        'type' => 'geo-point',2e3
                        'meta_data' => [
                            'label' => 'GPS coordinates',
                            'order' => 19
                        ]
                    ]
                ]
            ]*/
        ];

        foreach( $entries as $entry ) :

            $entry = ( object ) $entry;

            $form = new Form( Form::add( $entry->name ) );

            isset( $entry->module ) ? PostMeta::add( $form->id, 'module', $entry->module ) : null;

            foreach( ( isset( $entry->sections ) ? $entry->sections : [] ) as $index => $params ) :

                $params[ 'order' ] = isset( $params[ 'order' ] ) ? $params[ 'order' ] : ( $index + 1 );
                $params[ 'hidden' ] = isset( $params[ 'hidden' ] ) ? ( bool ) $params[ 'hidden' ] : false;

                if ( isset( $params[ 'name' ] ) ) :

                    $form_section = new FormSection( FormSection::add( $form->id, $params[ 'name' ], $params[ 'order' ], $params[ 'hidden' ] ) );

                    foreach( ( isset( $entry->sections[ $index ][ 'elements' ] ) ? $entry->sections[ $index ][ 'elements' ] : [] ) as $element_index => $element_params ) :

                        $element_params[ 'meta_data' ][ 'order' ] = isset( $element_params[ 'meta_data' ][ 'order' ] ) ? $element_params[ 'meta_data' ][ 'order' ] : ( $element_index + 1 );

                        if ( isset( $element_params[ 'name' ] ) && isset( $element_params[ 'type' ] ) )
                            FormElement::add( $form_section->id, $element_params[ 'name' ], $element_params[ 'type' ], ( isset( $element_params[ 'meta_data' ] ) ? $element_params[ 'meta_data' ] : [] ) );

                    endforeach;

                endif;

            endforeach;

        endforeach;
    }

    public function run() {
        $this->seed_forms();
    }

}
