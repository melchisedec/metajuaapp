<?php

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;
use App\Entities\Taxonomy;
use App\Entities\Term;
use App\Entities\TermMeta;

class LocationsSeeder extends Seeder
{

    public function run()
    {
        $regionid = Taxonomy::add( 'Region', 'Regions' );
        $districtid = Taxonomy::add( 'District','Districts' );
        $villageid = Taxonomy::add( 'Village','Villages' );
        $subcountyid = Taxonomy::add( 'Subcounty','Subcounties' );


        $entries =
            [
                'central' => [
                    'name' =>'Central',
                    'taxonomy' => $regionid,
                    'locations'=> [
                        'district'=> [
                            'name' => 'Kampala' ,
                            'taxonomy'=> $districtid,
                            'meta' => [

                             ]
                        ],
                        'subcounty'=> [
                            'name' => 'CentralSC',
                            'taxonomy' => $subcountyid
                        ],
                        'village' => [
                            'name' => 'Kamwokya',
                            'taxonomy' => $villageid
                        ]
                    ]
                ],
                'west' => [
                    'name' =>'Western',
                    'taxonomy' => $regionid,
                    'locations'=> [
                        'district'=> [
                            'name' => 'Gisoro' ,
                            'taxonomy'=> $districtid
                        ],
                        'subcounty'=> [
                            'name' => 'Bunagana',
                            'taxonomy' => $subcountyid
                        ],
                        'village' => [
                            'name' => 'Nateete',
                            'taxonomy' => $villageid
                        ]
                    ]
                ],
                'east' => [
                    'name' =>'Eastern',
                    'taxonomy' => $regionid,
                    'locations'=> [
                        'district'=> [
                            'name' => 'Mbale' ,
                            'taxonomy'=> $districtid
                        ],
                        'subcounty'=> [
                            'name' => 'Busano',
                            'taxonomy' => $subcountyid
                        ],
                        'village' => [
                            'name' => 'Buyaka',
                            'taxonomy' => $villageid
                        ]
                    ]
                ],
                'northern' => [
                    'name' =>'Northern',
                    'taxonomy' => $regionid,
                    'locations'=> [
                        'district'=> [
                            'name' => 'Paidha' ,
                            'taxonomy'=> $districtid,

                        ],
                        'subcounty'=> [
                            'name' => 'Pader_SC',
                            'taxonomy' => $subcountyid
                        ],
                        'village' => [
                            'name' => 'Tangi',
                            'taxonomy' => $villageid
                        ]
                    ]
                ]
           ];

        foreach( $entries as $key=>$value ):
            $entry =  ( object ) $value;
            Term::add( $entry->taxonomy, $entry->name );
            
            foreach( $entry->locations as $places => $location ):

                    $location = ( object ) $location;

                    Term::add( $location->taxonomy, $location->name );

                    $term = new Term( $location->taxonomy, $location->name );
                    
                if( $places == 'district' )
                       TermMeta::add( $term->id, 'region',  Term::add( $entry->taxonomy, $entry->name ) );

            endforeach;

        endforeach;


    }
}
