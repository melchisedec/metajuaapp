<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    public function run() {
        $this->call( 'AccountsSeeder' );
        $this->call( 'UsersSeeder' );
        $this->call( 'ModulesSeeder' );
        $this->call( 'FormsSeeder' );
        $this->call( 'LocationsSeeder' );
    }

}
